vpc_cidr_block = "10.0.0.0/16"
subnet_cidr_block = "10.0.10.0/24"
avail_zone = "us-west-2b"
env_prefix = "dev"
instance_type = "t2.micro"
my_ip = "73.15.16.40/32"
public_key_location = "/Users/nathanparks/.ssh/id_rsa.pub"
//image_name = "amzn2-ami-hvm-*-x86_64-gp2"